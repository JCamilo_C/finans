import styled from '@emotion/styled';

export const Tile = styled.div({
    borderRadius: '15px',
    height:'100%',
    width:'100%',
    color: '#000'
})

export const ResumeTile = styled(Tile)({
    background: '#323232',
    borderRadius: '15px 15px 0 0 ',
    color: '#fff'
})

export const SingleTile = styled(Tile)({
    background: '#D8E3E7',
    gridColumn: '1/3',
    gridRow: '1/3',
})

export const CategoryTile = styled(Tile)({
    background: '#F3F4ED',
    gridColumn: '3/8',
    gridRow: '1/6',
})