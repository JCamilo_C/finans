import styled from '@emotion/styled';

export const CardTemplate = styled.div<{width?: string, height?: string, padding?: string, margin?: string}>`
    position: relative;
    padding: ${props => props.padding ? props.padding : '15px'};
    border-radius: 25px;
    height: ${props => props.height ? props.height : '190px'};
    width: ${props => props.width ? props.width : '400px'};
    margin: ${props => props.margin ? props.margin : '0 10px'};
    text-align: start;
    overflow: auto;
`;

export const CardGrid = styled.div({
    display: 'grid',
    gridTemplateColumns: '1fr 1fr 1fr',
    gridTemplateRows:  '25% 40% 35%',
    width: '100%',
    height: '100%'
});

export const CardTypeText = styled.h6`
    grid-column: 1/3; 
    text-align: start;
    margin: auto 0; 
    textTransform: capitalize;
`;

export const CardAmountText = styled.h1`
    font-size: 25px;
    font-weight: 800;
    margin: auto 0px;
    grid-column: 1/4;  
`;

export const CreditConfigInfo = styled.p({
    fontSize: '18px',
    textAlign: 'start'
})
