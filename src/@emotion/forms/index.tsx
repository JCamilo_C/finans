import styled from '@emotion/styled'
import { IonItem } from '@ionic/react'

export const FormItem = styled(IonItem)({
    margin: '10px 0px'
})

export const FormSelectionItem = styled(FormItem)({
    width: '100%',
    gridTemplateColumns: '40% 60%'
})