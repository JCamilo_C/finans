import {
  IonApp
} from '@ionic/react';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';
/* Theme variables */
import './theme/variables.css';
import './theme/global.css'


import store from './redux/app.reducer';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import Pages from './pages';



const App: React.FC = () => {

  return (
    <IonApp>
      <Provider store={store.store}>
        <PersistGate loading={null} persistor={store.persistor}>
          <Pages />
        </PersistGate>
      </Provider>
    </IonApp>)
};

export default App;
