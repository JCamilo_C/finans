import { defaultValidators } from "react-reforms"
import {ICard } from "src/models/card.model"
import { LogoFranchises, TypeAccountCards, TypeFranchises } from "src/models/constants"

export const setFranchise = (card: ICard) => {
    switch (card.accountType) {
        case TypeAccountCards.CREDIT_TYPE:
            return <img src={`../../../assets/img/banks/${LogoFranchises[card.creditCard.franchise]}`} alt="bankBrand" className="bank-brand" />
        case TypeAccountCards.LOAN_TYPE:
                return <img src={`../../../assets/icon/loan.svg`} alt="bankBrand" className="bank-brand" />
        case TypeAccountCards.ACCOUNT_TYPE:
            return <img src={`../../../assets/icon/account.svg`} alt="bankBrand" className="bank-brand" />
        default:
            return <img src={`../../../assets/icon/money.svg`} alt="bankBrand" className="bank-brand" />
    }
}


export const configuratorAccountType = {
    [TypeAccountCards.CREDIT_TYPE]: {
        franchise: {
            value: TypeFranchises.AMERICAN_EXPRESS,
            validators: [],
            class: ''
        },
        month: {
            value: 1,
            validators: [defaultValidators.Min(1), defaultValidators.Max(31)],
            class: ''
        },
        year: {
            value: 21,
            validators: [defaultValidators.Min(new Date().getFullYear()), defaultValidators.Max((new Date().getFullYear() + 25))],
            class: ''
        },
        cut: {
            value: 15,
            validators: [defaultValidators.Min(1), defaultValidators.Max(31)],
            class: ''
        },
        limit: {
            value: 0,
            validators: [defaultValidators.Min(0)],
            class: ''
        }, 
        lastDigits: {
            value: 'XXXX',
            validators: [defaultValidators.Required()],
            class: ''
        },
    },
    [TypeAccountCards.LOAN_TYPE]: {
        createdAt: {
            value: new Date(),
            validators: [defaultValidators.Required()],
            class: ''
        },
        repeatOn: {
            value: 15,
            validators: [defaultValidators.Required()],
            class: ''
        },
        duration: {
            value: 3,
            validators: [defaultValidators.Required(), defaultValidators.Min(1)],
            class: ''
        },
        quota: {
            value: 100,
            validators: [defaultValidators.Required(), defaultValidators.Min(1)],
            class: ''
        },
        dues: {
            value: 1000,
            validators: [defaultValidators.Required(), defaultValidators.Min(1)],
            class: ''
        },
        fee: {
            value: 0.12,
            validators: [defaultValidators.Required(), defaultValidators.Min(1)],
            class: ''
        },
        lender: {
            value: 'Jhon Doe',
            validators: [defaultValidators.Required(), defaultValidators.Min(1)],
            class: ''
        },
    },
    [TypeAccountCards.ACCOUNT_TYPE]: {
        bank: {
            value: "",
            validators: [defaultValidators.Required()],
            class: ''
        },
        limit: {
            value: 15,
            validators: [defaultValidators.Required()],
            class: ''
        },
        lastDigits: {
            value: "",
            validators: [defaultValidators.Required(), defaultValidators.Min(1)],
            class: ''
        }
    },
}

export const selectAccountTypeOpt =  (header: string) => ({
    cssClass: 'custom-alert-css',
    header
})