
function getRandomHEXColor() {
    const SEED = '0123456789abcdef';
    let output = '#';
    while (output.length < 7) {
      output += SEED[Math.floor(Math.random() * SEED.length)];
    }
    return output;
  }
  

export const randomGradient = () => {
    var Color1 = getRandomHEXColor(),
        Color2  = getRandomHEXColor();
    return {Color1, Color2}
  }