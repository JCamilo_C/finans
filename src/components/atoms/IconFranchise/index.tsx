import React from 'react'
import { LogoFranchises, TypeAccountCards } from 'src/models/constants';

const SrcImage = {
    [TypeAccountCards.CREDIT_TYPE]: (franchise) => `/assets/img/banks/${LogoFranchises[franchise]}`,
    [TypeAccountCards.LOAN_TYPE]: "/assets/icon/loan.svg",
    [TypeAccountCards.ACCOUNT_TYPE]: "/assets/icon/account.svg"
}

export interface IconFranchiseProps {
    accountType: string,
    franchise: string
}
 
const IconFranchise: React.FC<IconFranchiseProps> = ({accountType, franchise}) => {
    const [srcIcon, setsrcIcon] = React.useState("../../../assets/icon/money.svg");

    React.useEffect(() => {
        if (accountType === TypeAccountCards.CREDIT_TYPE) {
            setsrcIcon(SrcImage[accountType](franchise));
        } else if (accountType !== TypeAccountCards.CASH_TYPE ) {
            setsrcIcon(SrcImage[accountType]);
        }
    }, [])

    return ( 
        <img src={srcIcon} alt="bankBrand" className="bank-brand" />
    );
}
 
export default IconFranchise;