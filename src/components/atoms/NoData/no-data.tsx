import React from 'react'
import { FormattedMessage } from 'react-intl';


export interface NoDataProps {
    idMessage: string;
    containerClasses: string;
    textClasses: string;
}
 
const NoData: React.SFC<NoDataProps> = ({idMessage, containerClasses = "default-no-data", textClasses = "default-text"}) => {
    return (  
        <div className={containerClasses}>
            <h1 className={textClasses}>
                <FormattedMessage id={idMessage} />
            </h1>
        </div>
    );
}
 
export default NoData;