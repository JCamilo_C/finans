import React from 'react'
import './svg.style.css'

export interface SvgCardBackgroundProps {
    colors: {Color1: string, Color2: string },
    styles: string
}
 
const SvgCardBackground: React.FC<SvgCardBackgroundProps> = ({colors, styles}) => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" version="1.1" xmlnsXlink="http://www.w3.org/1999/xlink" style={{background: colors.Color2}} className="card-background" width="1440" height="560" preserveAspectRatio="none" viewBox="0 0 1440 560">
            <g mask="url(&quot;#SvgjsMask1007&quot;)" fill="none">
                <rect width="1440" height="560" x="0" y="0" fill={colors.Color1}></rect>
                <path d="M 0,340 C 96,295.2 288,114.6 480,116 C 672,117.4 768,350 960,347 C 1152,344 1344,150.2 1440,101L1440 560L0 560z" fill="rgba(24, 74, 126, 1)"></path>
            </g>
            <defs>
                <mask id="SvgjsMask1007">
                    <rect width="1440" height="560" fill={colors.Color2}></rect>
                </mask>
            </defs>
        </svg>
    );
}
 
export default SvgCardBackground;