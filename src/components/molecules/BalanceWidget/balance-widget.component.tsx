import * as React from 'react';
import './balance.style.css'
import { connect } from 'react-redux';
import { getMovesByDate, getAmountMovesByDate } from '../../../redux/selectors/movement.selector';
import { Movement } from '../../../models/movement.model';
import { AddMovement } from '../../../redux/actions/movement.actions';
import { AppState } from '../../../redux/app.reducer';
import { getSelectCard } from '../../../redux/selectors';
import { ICard } from '../../../models/card.model';
import  moment from 'moment';
import { ResumeTile } from 'src/@emotion/balance';
import ListMoves from '../ListMoves/list-moves';
import NoData from 'src/components/atoms/NoData/no-data';

interface BalanceWidgetProps  {
    lastMoves: Movement[],
    monthAmount: number,
    cardSelected: ICard
}

const BalanceWidget: React.FC<BalanceWidgetProps> = ({ lastMoves, monthAmount, cardSelected }) => {
    const [segmentDate, setSegmentDate] = React.useState<string>('')
    React.useEffect(() => {
        moment.locale('es');
        const now = new Date();
        const firstDay = moment(new Date(now.getFullYear(), now.getMonth(), 1, 0, 0, 0)).format('MMMM DD');
        const lastDay = moment(new Date(now.getFullYear(), now.getMonth() + 1, 0, 23, 59, 59)).format('MMMM DD');
        setSegmentDate(`${firstDay} - ${lastDay} `)

    }, [])

    return (
        <div className="balance-grid">
            <div className="resume-widget">
                <ResumeTile>
                    <div className="balance-default-grid" style={{padding: "0 12px"}}>
                        <h1 className="tile-title">Resumen Mensual</h1>
                    </div>
                    <div className="balance-default-grid" style={{padding: "0 12px"}}>
                        <p style={{gridColumn:"1/3"}}>{segmentDate}</p>
                        <p style={{gridColumn:"3/4", textAlign: 'end'}}>
                            {cardSelected ? new Intl.NumberFormat('de-DE', { style: 'currency', currency: cardSelected.currency }).format(monthAmount) : '$ 0'}
                            
                        </p> 
                    </div>
                    { 
                        lastMoves.length === 0 ? 
                            <NoData idMessage="nodata.message.movement" containerClasses="balance-default-grid no-data-container" textClasses="no-data-text" /> 
                            : <ListMoves lastMoves={lastMoves} />
                    }
                </ResumeTile>
            </div>
        </div>
    )
} 


const mapStateProps = (state: AppState) => {
    let date = new Date();
    return  {
        lastMoves: getMovesByDate(date)(state),
        monthAmount: getAmountMovesByDate(state),
        cardSelected: getSelectCard(state)
    }
}

export default connect(mapStateProps, {AddMovement})(BalanceWidget);