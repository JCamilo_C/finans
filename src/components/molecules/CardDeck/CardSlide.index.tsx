import { IonSlide } from '@ionic/react';
import React from 'react'
import { ICard } from 'src/models/card.model';
import { randomGradient } from 'src/Auxiliary/randoms';
import CardItem from 'src/components/molecules/CardItem';
import './CardDeck.style.css'

export interface CardItemsProps {
    active: string,
    card: ICard
}
 
const CardSlide: React.FC<CardItemsProps> = ({card}) => {
    const config = {
        width: '400px', 
        height: '190px', 
        padding: '15px', 
        margin: '0 10px'
    }
    
    return (
        <IonSlide>
            <CardItem card={card} background={randomGradient()} config={config} />
        </IonSlide>
    );
}
 
export default CardSlide;