import React from 'react'
import { FormattedMessage } from 'react-intl';
import NumberFormat from 'react-number-format';
import { CardAmountText, CardGrid, CardTemplate, CardTypeText, CreditConfigInfo } from 'src/@emotion/cards';
import { randomGradient } from 'src/Auxiliary/randoms';
import IconFranchise from 'src/components/atoms/IconFranchise';
import SvgCardBackground from 'src/components/atoms/SvgCardBackground';

import { ICard } from 'src/models/card.model';
import { TypeAccountCards } from 'src/models/constants';
import "./card.style.css"

export interface CardProps {
    card: ICard,
    background?: {Color1: string, Color2: string},
    config?: {width?: string, height?: string, padding?: string, margin?: string}
}
 
const CardItem: React.FC<CardProps> = ({card, config=null, background=null}) => {
    const franchise = React.useRef(null);
    const backgroundRef = React.useRef(background);

    React.useEffect(() => {
        if (card.accountType === TypeAccountCards.CREDIT_TYPE) franchise.current = card.creditCard.franchise;
        if (!background) backgroundRef.current = randomGradient();
    }, []);

    return (
        <CardTemplate {...config} >
            <CardGrid>
                <SvgCardBackground colors={backgroundRef.current} styles="card-background"  />
                <CardTypeText>
                    <FormattedMessage id={card.accountType} />
                </CardTypeText>
                <IconFranchise accountType={card.accountType} franchise={franchise.current} />
                <CardAmountText>
                    <NumberFormat
                        value={card.amount}
                        displayType={'text'}
                        thousandSeparator={true}
                        prefix={'$'}
                        suffix={card.currency}
                    />
                </CardAmountText>
                <CreditConfigInfo style={{gridColumn: '1/3'}}>{card.accountName}</CreditConfigInfo>
                {
                    card.accountType === TypeAccountCards.CREDIT_TYPE ? (
                        <CreditConfigInfo style={{gridColumn: '3/4', marginLeft: 'auto'}}>{card.creditCard.expire}</CreditConfigInfo>
                    ): null
                }
            </CardGrid>
        </CardTemplate>
    );
}
 
export default CardItem;