
import React, { Dispatch, SetStateAction, useEffect, useRef} from 'react';
import { IonSlides, IonIcon} from '@ionic/react';
import { ICard } from '../../../models/card.model';
import './CardSlidesWidget.style.css'
import {connect} from 'react-redux';
import { getSelectIndxCard } from '../../../redux/selectors';
import { ChangeCard } from '../../../redux/actions/card.actions';
import { addOutline} from 'ionicons/icons';
import { useHistory } from 'react-router';
import CardSlide from '../CardDeck/CardSlide.index';


    
interface CardSlidesWidgetProps {
    cards: ICard[],
    setOpenModal: Dispatch<SetStateAction<boolean>>,
    selectedCard: number,
    ChangeCard: (numberCard: number) => {},
}

const CardSlidesWidget: React.FC<CardSlidesWidgetProps> = ({cards, selectedCard, ChangeCard, setOpenModal}) => {
    let slideOpts = {
        initialSlide: 0,
        speed: 400,
        slidesPerView: 1,
        centeredSlides: false,
        freeMode: false
    
    };
    const history = useHistory()
    const [cardList, setCardList] = React.useState<JSX.Element[]>([])
    const ionSlide = useRef<HTMLIonSlidesElement>(null)

    useEffect(() => {
        setCardList([]);
        const cardItems: JSX.Element[] = []
        cards.filter(c => c.default).forEach( (card, idx) => {
            cardItems.push(<CardSlide key={idx} card={card} active={'active'} />)
        })
        setCardList(cardItems)
    }, [])

    const changeSlide = React.useCallback( async () => {
        const swiper = await ionSlide.current.getSwiper();
        let indexSlide = swiper.activeIndex;
        if (selectedCard !== indexSlide && indexSlide !== cards.length) ChangeCard(indexSlide)
    }, [])


    const createCard = () => history.push("/cards/create");
    
    useEffect( () => {
        async function refreshSlides() {
           const swiper = await ionSlide.current.getSwiper()
           swiper.slideTo(0)
        }
        refreshSlides();
    }, [cards])

    return (
        <div className="card-position">
            <div style={{margin: 'auto'}}>
                <div className="add-card" onClick={() => setOpenModal(true)} >
                    <div  style={{display: 'flex',height: '100%', width: '100%'}}>
                        <IonIcon className="text-dark" icon={addOutline} style={{fontSize:'xxx-large', height: '20px'}} />
                    </div>
                </div>
            </div>
            <IonSlides ref={ionSlide} options={slideOpts} onIonSlideTouchEnd={changeSlide} >
                { cardList.map( (card) => card) }
            </IonSlides>
        </div>
        
    )
}

const mapStateToProps = (state: any) => {
    return {
      selectedCard: getSelectIndxCard(state)
  
    }
  }
  
export default connect(mapStateToProps, {ChangeCard})(CardSlidesWidget);
