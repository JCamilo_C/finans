import React from 'react';
import styled from '@emotion/styled'
const UserName = styled.p({
    fontWeight: 'bold',
    fontFamily: 'Nunito, sans-serif',
    gridColumn: '1/5',
    margin: '0 5px',
    textAlign: 'end'
  })

  const UserEmail = styled.p({
    gridColumn: '1/5',
    margin: '0 5px',
    textAlign: 'end'
  })

  const UserAvatar = styled.div({
    gridColumn: '5/6',
    gridRow: '1/3',
    textAlign: 'center'
  })

  const Avatar = styled.div<{avatar: string}>`
    background-image: url(../../../../assets/img/${ props => props.avatar});
  `;

  const UserInfo = styled.div({
    padding: "5px",
    color: "#fff",
    fontSize: '12px',
    gridColumn: '2/4',
    display: 'grid',
    gridTemplateColumns: 'repeat(5, 1fr)',
    gridTemplateRows: 'repeat(2, 1fr)'
  })

export interface HeaderProps {
    userName: string;
    userEmail: string;
    avatar: string;
}
 
const Header: React.FC<HeaderProps> = ({userName, userEmail, avatar}) => {
    return ( 
        <div className="container-user-view" style={{padding: '15px 5px'}}>
          <h1 style={{padding: '0', margin: '10px',}}>Finans</h1>
          <UserInfo>
            <UserName>
              {userName}
            </UserName>
            <UserEmail>
              {userEmail}
            </UserEmail>
            <UserAvatar>
              <Avatar className="avatar avatar-1" avatar={avatar}/>
            </UserAvatar>
          </UserInfo>
        </div>
     );
}
 
export default Header;