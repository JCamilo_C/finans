import { IonAvatar, IonIcon, IonItem, IonLabel, IonList } from '@ionic/react';
import { arrowDownOutline, arrowUpOutline } from 'ionicons/icons';
import React from 'react'
import NumberFormat from 'react-number-format';
import { Movement } from 'src/models/movement.model';
import Utils from 'src/utils';

export interface ListMovesProps {
    lastMoves: Movement[]
}
 
const ListMoves: React.FC<ListMovesProps> = ({lastMoves}) => {
    return (
        <IonList id="moves-resume" className="productList" lines='none' no-lines>
            {
                lastMoves.map( (move, idx) => (
                    <IonItem key={idx}>
                        <IonAvatar slot="start">
                            {Utils.getSignNum(move.amount, <IonIcon  className="avatar-icon-success" icon={arrowUpOutline} />, <IonIcon className="avatar-icon-danger" icon={arrowDownOutline} />)}
                        </IonAvatar>
                        <IonLabel>
                            <h2>{move.title}</h2>
                            <div className="general-grid">
                                <p style={{gridColumn:"1/3"}}>
                                <NumberFormat
                                        value={move.amount}
                                        displayType={'text'}
                                        thousandSeparator={true}
                                        prefix={'$'}// <--- Don't forget this!
                                    />
                                </p>
                                <small style={{gridColumn:"3/4", margin: 'auto'}}>{move.createdAt.toDateString()}</small> 
                            </div>
                        </IonLabel>
                    </IonItem>
                ))
            }
        </IonList>
    );
}
 
export default ListMoves;