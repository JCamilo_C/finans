import { SIDE_NAV_LIGHT, NAV_TYPE_SIDE } from '../constants/ThemeConstant';
import { env } from './EnvironmentConfig'

export const APP_NAME = 'Finans';
export const API_BASE_URL = env.API_ENDPOINT_URL
export const APP_PREFIX_PATH = '/';
export const COMPANY_PREFIX_PATH = '/app/store';
export const HOME_PREFIX_PATH = '/dashboard/tabHome';
export const INTRODUCTION_PREFIX_PATH = '/introduction';

export const THEME_CONFIG = {
	navCollapsed: false,
	sideNavTheme: SIDE_NAV_LIGHT,
	locale: 'en',
	navType: NAV_TYPE_SIDE,
	topNavColor: '#3e82f7',
	headerNavColor: '#ffffff',
	mobileNav: false,
	currentTheme: 'light',
	currency: 'USD',
};

// Create our number formatter.
export var formatter = new Intl.NumberFormat(THEME_CONFIG.locale, {
	style: 'currency',
	currency: THEME_CONFIG.currency,
  
	// These options are needed to round to whole numbers if that's what you want.
	//minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
	//maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
});