import firebase from 'firebase';
import {FirebaseConfig, FirebaseTables} from './Firebase.config';

firebase.initializeApp(FirebaseConfig);
const firestore = firebase.firestore();


export const createDoc = async <T>(data: T): Promise<{id: string, data: T}> => {
    return await firestore.collection(FirebaseTables.USERS)
        .add(data)
        .then( (docRef) => ({id: docRef.id, data}))
        .catch( err => null);
}


