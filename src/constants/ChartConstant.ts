import { ApexOptions } from "apexcharts";

export const COLOR_1 = '#3e82f7';  // blue
export const COLOR_2 = '#04d182';  // cyan
export const COLOR_3 = '#ff6b72';  // volcano
export const COLOR_4 = '#ffc107';  // gold
export const COLOR_5 = '#a461d8';  // purple
export const COLOR_6 = '#fa8c16';  // orange
export const COLOR_7 = '#17bcff';  // geekblue

export const COLOR_1_LIGHT = 'rgba(62, 130, 247, 0.15)';
export const COLOR_2_LIGHT = 'rgba(4, 209, 130, 0.1)';
export const COLOR_3_LIGHT = 'rgba(222, 68, 54, 0.1)';
export const COLOR_4_LIGHT = 'rgba(255, 193, 7, 0.1)';
export const COLOR_5_LIGHT = 'rgba(139, 75, 157, 0.1)';
export const COLOR_6_LIGHT = 'rgba(250, 140, 22, .1)';
export const COLOR_7_LIGHT = 'rgba(23, 188, 255, 0.15)';

export const COLORS = [
	COLOR_1,
	COLOR_2,
	COLOR_3,
	COLOR_4,
	COLOR_5,
	COLOR_6,
	COLOR_7
]

export const COLORS_LIGHT = [
	COLOR_1_LIGHT,
	COLOR_2_LIGHT,
	COLOR_3_LIGHT,
	COLOR_4_LIGHT,
	COLOR_5_LIGHT,
	COLOR_6_LIGHT,
	COLOR_7_LIGHT
]

export const COLOR_AXES = '#edf2f9';
export const COLOR_TEXT = '#455560';

export const apexLineChartDefaultOption = {
	chart: {
		zoom: {
			enabled: false
		},
		toolbar: {
			show: false
		}
	},
	colors: [...COLORS],
	dataLabels: {
		enabled: false
	},
	stroke: {
		width: 3,
		curve: 'smooth',
		lineCap: 'round'
	},
	legend: {
		position: 'top',
		horizontalAlign: 'right',
		offsetY: -15,
		itemMargin: {
			vertical: 20
		},
		tooltipHoverFormatter: function(val: any, opts: any) {
			return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
		}
	},
	xaxis: {
		categories: [],
	},
	grid: {
		xaxis: {
			lines: {
				show: true,
			}
		},
		  yaxis: {
			lines: {
			  show: false,
			}
		},
	}
}

export const apexAreaChartDefaultOption = {...apexLineChartDefaultOption}

export const apexBarChartDefaultOption = {
	chart: {
		zoom: {
			enabled: false
		},
		toolbar: {
			show: false
		}
	},
	plotOptions: {
		bar: {
			horizontal: false,
			columnWidth: '25px',
			startingShapre: 'rounded',
			endingShape: 'rounded'
		},
	},
	colors: [...COLORS],
	dataLabels: {
		enabled: false
	},
	stroke: {
		show: true,
		width: 6,
		curve: 'smooth',
		colors: ['transparent']
	},
	legend: {
		position: 'top',
		horizontalAlign: 'right',
		offsetY: -15,
		inverseOrder: true,
		itemMargin: {
			vertical: 20
		},
		tooltipHoverFormatter: function(val: any, opts: any) {
			return val + ' - ' + opts.w.globals.series[opts.seriesIndex][opts.dataPointIndex] + ''
		}
	},
	xaxis: {
		categories: [],
	},
	fill: {
		opacity: 1
	},
	tooltip: {
		y: {
			formatter: (val: string) => (`${val}`)
		}
	}
}

export const apexRadarChartDefaultOptions: ApexOptions ={
	chart: {
		type: 'radar',
		dropShadow: {
			enabled: true,
			blur: 1,
			left: 1,
			top: 1
		}
	},
	plotOptions: {
		radar: {
		  	polygons: {
				strokeColors: '#e8e8e8',
				fill: {
					colors: ['#f8f8f8', '#fff']
				}
		  }
		}
	},
	colors: [...COLORS],
	dataLabels: {
		enabled: false
	},
	tooltip: {
		y: {
			formatter: (val: number) => (`${val}`)
		}
	},
	stroke: {
		show: true,
		width: 2,
		curve: 'smooth',
	},
	fill: {
		opacity: 0.5
	},
	markers: {
		size: 0
	},
	xaxis: {
		categories: ['Categoria 1', 'Categoria 2', 'Categoria 3', 'Categoria 4', 'Categoria 5', 'Categoria 6', 'Categoria 7', 'Categoria 8', 'Categoria 9', 'Categoria 10'],
		labels: {
			show: false,
			style: {
				fontFamily: 'Nunito',
				fontWeight: '500',
				colors: [...Array(10).fill((x: any)=>x).map(() => '#fff')],
			}
		},
	}

}; 

export const apexHeatChartDefaultOptions: ApexOptions = {
	chart: {
		type: 'heatmap',
	},
	plotOptions: {
		heatmap: {
			shadeIntensity: 0.5,
			radius: 0,
			useFillColorAsStroke: true,
			colorScale: {
				ranges: [
					{
						from: -30,
						to: 5,
						name: 'low',
						color: '#00A100'
					},
					{
						from: 6,
						to: 20,
						name: 'medium',
						color: '#128FD9'
					},
					{
						from: 21,
						to: 45,
						name: 'high',
						color: '#FFB200'
					},
					{
						from: 46,
						to: 55,
						name: 'extreme',
						color: '#FF0000'
					}
				]
			}
		}
	},
	dataLabels: {
		enabled: false
	},
	stroke: {
		width: 1
	},
};

export const apexPieChartDefaultOption = {
	colors: [...COLORS],
	plotOptions: {
		pie: {
			size: 50,
			donut: {
				labels: {
					show: true,
					total: {
						show: true,
						showAlways: true,
						label: '',
						fontSize: '18px',
						fontFamily: 'Roboto',
						fontWeight: 'bold',
						color: '#1a3353',
						formatter: function (w: any) {
							return w.globals.seriesTotals.reduce((a: any, b: any) => {
								return a + b
							}, 0)
						}
					}
				},
				size: '87%'
			}
		}
	},
	labels: [],
	dataLabels: {
		enabled: false
	},
	legend: {
		show: false
	}
}

export const apexSparklineChartDefultOption: ApexOptions = {
	chart: {
		type: 'line',
		sparkline: {
			enabled: true
		}
	},
	stroke: {
		width: 2,
		curve: 'smooth'
	},
	tooltip: {
		fixed: {
		  	enabled: false
		},
		x: {
		  	show: false
		},
		y: {
			title: {
				formatter: function (seriesName) {
					return ''
				}
			}
		},
		marker: {
		  show: false
		}
	}
}