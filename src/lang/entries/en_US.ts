

import actions from '../../assets/locales/en_US/actions.json';
import types from '../../assets/locales/en_US/types.json';
import messages from '../../assets/locales/en_US/message.json';
import forms from '../../assets/locales/en_US/forms.json';

const EnLang = {
  locale: 'en-US',
  messages: {
    ...types,
    ...actions,
    ...messages,
    ...forms
  },
};
export default EnLang;
