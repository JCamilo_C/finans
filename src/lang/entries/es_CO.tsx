
import actions from '../../assets/locales/es_CO/actions.json';
import types from '../../assets/locales/es_CO/types.json';
import messages from '../../assets/locales/es_CO/message.json';
import forms from '../../assets/locales/es_CO/forms.json';

const EsLang = {
  locale: 'es-ES',
  messages: {
    ...types,
    ...actions,
    ...messages,
    ...forms
  },
};
export default EsLang;
