import enLang from './entries/en_US';
import esLang from './entries/es_CO';

const AppLocale = {
    en: enLang,
    es: esLang,
};

export default AppLocale;