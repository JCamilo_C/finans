import React from 'react'
import { IntlShape } from 'react-intl';
import { useForm } from 'react-reforms';
import { FormItem } from 'src/@emotion/forms';
import { configuratorAccountType } from 'src/Auxiliary/constants';
import { TypeAccountCards } from 'src/models/constants';

interface AccountFormTypeProps {
    intl: IntlShape,
    changeSlide: (side: "next" | "prev") => Promise<void>
}
 
const AccountFormType: React.FC<AccountFormTypeProps> = ({intl, changeSlide}) => {
    const {values, ValidateInput} = useForm(configuratorAccountType[TypeAccountCards.ACCOUNT_TYPE], {});


    return (
        <React.Fragment>
            <FormItem lines="none">
                <div className="form-control">
                    <label htmlFor="bank" style={{marginBottom: '4px'}} >{intl.formatMessage({id: "forms.account.bank"})}</label>
                    <input id="bank" name="bank" onChange={e => ValidateInput(e.target.name, e.target.value)} />
                </div>
            </FormItem>
            <FormItem lines="none">
                <div className="form-control">
                    <label htmlFor="limit" style={{marginBottom: '4px'}} >{intl.formatMessage({id: "forms.account.limit"})}</label>
                    <input id="limit" name="limit" onChange={e => ValidateInput(e.target.name, e.target.value)} />
                </div>
            </FormItem>
            <FormItem lines="none">
                <div className="form-control">
                    <label htmlFor="lastDigits" style={{marginBottom: '4px'}} >{intl.formatMessage({id: "forms.credit.lastDigits"})}</label>
                    <input id="lastDigits" name="lastDigits" onChange={e => ValidateInput(e.target.name, e.target.value)} />
                </div>
            </FormItem>
        </React.Fragment>
    );
}
 
export default AccountFormType;