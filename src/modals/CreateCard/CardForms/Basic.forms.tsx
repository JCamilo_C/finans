import { IonSelect, IonSelectOption } from '@ionic/react';
import React, { Dispatch, SetStateAction } from 'react'
import { FormattedMessage, IntlShape } from 'react-intl';
import { defaultValidators, useForm } from 'react-reforms';
import { FormItem, FormSelectionItem } from 'src/@emotion/forms';
import { configuratorAccountType, selectAccountTypeOpt } from 'src/Auxiliary/constants';
import { ICard } from 'src/models/card.model';
import { TypeAcceptedCurrencies, TypeAccountCards } from 'src/models/constants';

interface BasicCardProps {
    intl: IntlShape,
    setcardState: Dispatch<SetStateAction<ICard>>,
    cardState: ICard,
    changeSlide: (side: "next" | "prev") => Promise<void>,
    setOpenModal: Dispatch<SetStateAction<boolean>>,
    setExtraParams: Dispatch<SetStateAction<any>>,
}

const formStructure = {
    accountName: {
        value: "",
        validators: [defaultValidators.Required()],
        class: ''
    }, 
    accountType: {
        value: TypeAccountCards.ACCOUNT_TYPE,
        validators: [defaultValidators.Required()],
        class: ''
    },
    amount: {
        value: 0,
        validators: [defaultValidators.Required(), defaultValidators.Min(0)],
        class: ''
    }, 
    currency: {
        value: TypeAcceptedCurrencies.AMERICA,
        validators: [defaultValidators.Required()],
        class: ''
    },
    default: {
        value: false,
        validators: [],
        class: ''
    },
    maxValue: {
        value: 0,
        validators: [],
        class: ''
    },
    midValue: {
        value: 0,
        validators: [],
        class: ''
    },
    minValue: {
        value: 0,
        validators: [defaultValidators.Min(0)],
        class: ''
    },
}
 
const BasicCardForm: React.FC<BasicCardProps> = ({intl, setcardState, cardState, changeSlide, setOpenModal, setExtraParams}) => {
    const {values, ValidateInput} = useForm(formStructure, {customClass: {error: 'error', success: 'success'}})

    function handleInput<Type>(name: string, value: Type | TypeAccountCards){
        
        if (name === 'accountType') {
            setExtraParams(configuratorAccountType[value as TypeAccountCards]);
        }
        ValidateInput(name, value)

        setcardState({
            ...cardState,
            [name]: value
        })
    }

    return ( 
        <React.Fragment>
            <FormItem lines="none">
                <div className="form-control">
                    <label htmlFor="accountName" style={{marginBottom: '4px'}} >Nombre de cuenta</label>
                    <input 
                        id="accountName" 
                        name="accountName" 
                        value={values.accountName.value}
                        onChange={e => handleInput(e.target.name, e.target.value)} />
                </div>
            </FormItem>
            <FormSelectionItem lines="none">
                <div className="selection-grid">

                    <div className="form-control" style={{gridColumn: '1/2'}}>
                        <label style={{marginBottom: '4px'}}>Tipo de cuenta</label>
                    </div>

                    <IonSelect 
                        style={{gridColumn: '2/4', maxWidth: "100%"}} 
                        name="accountType" 
                        className="form-input" 
                        value={values.accountType.value} 
                        okText={intl.formatMessage({id: "action.pick"})} 
                        cancelText={intl.formatMessage({id: "action.close"})}  
                        interfaceOptions={selectAccountTypeOpt('Seleccione un tipo de cuenta')} 
                        onIonChange={e => handleInput("accountType", e.detail.value)} >
                            <IonSelectOption value={TypeAccountCards.ACCOUNT_TYPE}>Cuenta</IonSelectOption>
                            <IonSelectOption value={TypeAccountCards.CASH_TYPE}>Efectivo / Bolsillo</IonSelectOption>
                            <IonSelectOption value={TypeAccountCards.CREDIT_TYPE}>Tarjeta de crédito</IonSelectOption>
                            <IonSelectOption value={TypeAccountCards.LOAN_TYPE}>Préstamo</IonSelectOption>
                    </IonSelect>

                </div>
            </FormSelectionItem>
            <FormItem lines="none">
                <div className="form-control">
                    <label htmlFor="amount" style={{marginBottom: '4px'}} >Valor inicial</label>
                    <input type="number" id="amount" name="amount" placeholder="Valor inicial" onChange={e => handleInput(e.target.name, e.target.value)} />
                </div>
            </FormItem>
            <FormSelectionItem lines="none">
                <div className="selection-grid">
                    <div className="form-control" style={{gridColumn: '1/2'}}>
                        <label style={{marginBottom: '4px'}}>Tipo de cuenta</label>
                    </div>
                    <IonSelect 
                        id="currency" 
                        name="currency" 
                        style={{gridColumn: '2/4', maxWidth: "100%"}} 
                        className="form-input"  
                        okText={intl.formatMessage({id: "action.pick"})} 
                        cancelText={intl.formatMessage({id: "action.close"})} 
                        interfaceOptions={selectAccountTypeOpt('Seleccione un tipo de moneda')}
                        value={values.currency.value}
                        onIonChange={e => handleInput("currency", e.detail.value)} >
                            <IonSelectOption value={TypeAcceptedCurrencies.AMERICA}>{TypeAcceptedCurrencies.AMERICA}</IonSelectOption>
                            <IonSelectOption value={TypeAcceptedCurrencies.BRAZILIAN}>{TypeAcceptedCurrencies.BRAZILIAN}</IonSelectOption>
                            <IonSelectOption value={TypeAcceptedCurrencies.BRITISH}>{TypeAcceptedCurrencies.BRITISH}</IonSelectOption>
                            <IonSelectOption value={TypeAcceptedCurrencies.COLOMBIAN}>{TypeAcceptedCurrencies.COLOMBIAN}</IonSelectOption>
                            <IonSelectOption value={TypeAcceptedCurrencies.EUROPEAN}>{TypeAcceptedCurrencies.EUROPEAN}</IonSelectOption>
                    </IonSelect>
                </div>
            </FormSelectionItem>
            <footer style={{margin: '2.5rem 2rem 0', textAlign: 'right', display: 'flex', flexDirection: 'row', justifyContent: "space-between"}}>
                <button type="button" className="link"  onClick={e => setOpenModal(false    )} style={{right: '2%', width: "12rem"}}>
                    <FormattedMessage id="action.close" />
                </button>
                <button type="button" className="primary"  onClick={e => changeSlide('next')} style={{right: '2%', width: "12rem"}}>
                    <FormattedMessage id="action.next" />
                </button>
            </footer>
        </React.Fragment>
     );
}
 
export default BasicCardForm;