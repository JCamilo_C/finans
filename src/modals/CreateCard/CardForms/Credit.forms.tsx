import React from 'react'
import { IonSelect, IonSelectOption } from '@ionic/react';
import { useForm } from 'react-reforms';
import { FormItem, FormSelectionItem } from 'src/@emotion/forms';
import { TypeAccountCards, TypeFranchises } from 'src/models/constants';
import { FormattedMessage, IntlShape } from 'react-intl';
import { configuratorAccountType } from 'src/Auxiliary/constants';

interface CreditCardFormProps {
    intl: IntlShape,
    changeSlide: (side: "next" | "prev") => Promise<void>
}

const CreditCardForm: React.FC<CreditCardFormProps> = ({intl, changeSlide}) => {

    const {values, ValidateInput} = useForm(configuratorAccountType[TypeAccountCards.CREDIT_TYPE], {});
    const selectOpt = {
        cssClass: 'custom-alert-css',
        header: intl.formatMessage({id: "message.introduction.form.dropdown"})
    }

    return (
        <React.Fragment>
            <FormSelectionItem lines="none">
                <div className="selection-grid">
                    <div className="form-control" style={{gridColumn: '1/2'}}>
                        <label htmlFor="franchise" style={{marginBottom: '4px'}}>{intl.formatMessage({id: "forms.credit.franchise"})} </label>
                    </div>
                    
                    <IonSelect 
                        id="franchise" 
                        name="franchise" 
                        style={{gridColumn: '2/4', maxWidth: "100%"}}  
                        okText={intl.formatMessage({id: "action.pick"})} 
                        cancelText={intl.formatMessage({id: "action.close"})} 
                        interfaceOptions={selectOpt} 
                        onIonChange={e => ValidateInput('currency', e.detail.value)} 
                        className={`form-input ${values.franchise.class}`} >
                            <IonSelectOption value={TypeFranchises.AMERICAN_EXPRESS}>{TypeFranchises.AMERICAN_EXPRESS}</IonSelectOption>
                            <IonSelectOption value={TypeFranchises.DINNERS_CLUB}>{TypeFranchises.DINNERS_CLUB}</IonSelectOption>
                            <IonSelectOption value={TypeFranchises.MASTERCARD}>{TypeFranchises.MASTERCARD}</IonSelectOption>
                            <IonSelectOption value={TypeFranchises.VISA}>{TypeFranchises.VISA}</IonSelectOption>
                            <IonSelectOption value={TypeFranchises.OTHER}>{TypeFranchises.OTHER}</IonSelectOption>
                    </IonSelect>
                </div>
            </FormSelectionItem>
            <FormItem lines="none">
                <div className="form-control">
                    <label style={{marginBottom: '4px'}}>{intl.formatMessage({id: "forms.credit.expire"})}</label>
                    <div style={{display: "flex", flexDirection: "row"}}>
                        <input id="month" name="month" placeholder={intl.formatMessage({id: "form.credit.month"})} style={{marginRight: "5px", width: "100%"}} onChange={e => ValidateInput(e.target.name, e.target.value)} />
                        <input id="year" name="year" placeholder={intl.formatMessage({id: "form.credit.year"})} style={{marginLeft: "5px", width: "100%"}} onChange={e => ValidateInput(e.target.name, e.target.value)} />
                    </div>
                </div>
            </FormItem>
            <FormItem lines="none">
                <div className="form-control">
                    <label htmlFor="cut" style={{marginBottom: '4px'}} >{intl.formatMessage({id: "forms.credit.cut"})}</label>
                    <input id="cut" name="cut" onChange={e => ValidateInput(e.target.name, e.target.value)} />
                </div>
            </FormItem>
            <FormItem lines="none">
                <div className="form-control">
                    <label htmlFor="accountName" style={{marginBottom: '4px'}} >{intl.formatMessage({id: "forms.credit.limit"})}</label>
                    <input id="limit" name="limit" onChange={e => ValidateInput(e.target.name, e.target.value)} />
                </div>
            </FormItem>
            <FormItem lines="none">
                <div className="form-control">
                    <label htmlFor="lastDigits" style={{marginBottom: '4px'}} >{intl.formatMessage({id: "forms.credit.lastDigits"})}</label>
                    <input id="lastDigits" name="lastDigits" onChange={e => ValidateInput(e.target.name, e.target.value)} />
                </div>
            </FormItem>
            <div style={{margin: '2.5rem 2rem', display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                <button type="button" className="primary"  onClick={e => changeSlide('prev')} style={{right: '2%', width: "12rem"}}>
                    <FormattedMessage id="action.back" />
                </button>
                <button type="button" className="primary"  onClick={e => changeSlide('next')} style={{right: '2%', width: "12rem"}}>
                    <FormattedMessage id="action.next" />
                </button>
            </div>
        </React.Fragment>
    )

}

export default CreditCardForm;