import React, { Dispatch, SetStateAction } from 'react';
import { IonContent, IonModal, IonSlide, IonSlides } from '@ionic/react';
import './create-card.style.css'
import { ICard } from 'src/models/card.model'
import { TypeAccountCards } from 'src/models/constants';
import CardItem from 'src/components/molecules/CardItem';
import { randomGradient } from 'src/Auxiliary/randoms';
import { useIntl } from 'react-intl';
import CreditCardForm from './CardForms/Credit.forms';
import { configuratorAccountType } from 'src/Auxiliary/constants';
import BasicCardForm from './CardForms/Basic.forms';
import AccountFormType from './CardForms/Account.forms';

export interface CreateCardModalProps {
    openModal: boolean;
    setOpenModal: Dispatch<SetStateAction<boolean>>;
    localCurrency: string;
}


 
const CreateCardModal: React.FC<CreateCardModalProps> = ({openModal, setOpenModal, localCurrency}) => {
    const CardForm: ICard = {
        accountName: '',
        accountType: TypeAccountCards.ACCOUNT_TYPE,
        amount: 0,
        currency: localCurrency,
        default: false
    }

    const intl = useIntl();
    
    const [cardState, setcardState] = React.useState(CardForm)
    const [extraForms, setExtraParams] = React.useState<TypeAccountCards>(TypeAccountCards.CREDIT_TYPE)

    const config = {
        width: 'auto', 
        padding: '15px', 
        margin: '25px 15px'
    }


    
    

    const handleDismiss = () => {
        setOpenModal(false);
    }

    const slider = React.useRef<HTMLIonSlidesElement>(null);

    const changeSlide = async (side: 'next' | 'prev') => {
        const swiper = await slider.current.getSwiper();
        if (side === 'next') {
            swiper.slideNext();
        } else {
            swiper.slidePrev();
        }
    }

    const slideOpts = {
        speed: 400,
        allowTouchMove: false,
        freeMode: false
    };

    const SelectExtraForm = () => {
        switch (extraForms) {
            case TypeAccountCards.CREDIT_TYPE:
                return <CreditCardForm intl={intl} changeSlide={changeSlide}/>;
            case TypeAccountCards.ACCOUNT_TYPE:
                    return <AccountFormType intl={intl} changeSlide={changeSlide}/>;
            default:
                break;
        }
    }

    return ( 
        <IonModal isOpen={openModal} cssClass='my-custom-class' keyboardClose={false} onDidDismiss={handleDismiss}>
            <IonContent>
                <CardItem card={cardState} background={randomGradient()} config={config} />
                <div className="panel-form">
                    <form className="form-card-grid " style={{padding: '16px 5px'}}>
                        <IonSlides options={slideOpts} ref={slider} className="card-form">
                            <IonSlide>
                                <BasicCardForm 
                                    intl={intl} 
                                    setcardState={setcardState} 
                                    cardState={cardState} 
                                    changeSlide={changeSlide} 
                                    setExtraParams={setExtraParams} 
                                    setOpenModal={setOpenModal} />
                            </IonSlide>
                            <IonSlide>
                                <CreditCardForm intl={intl} changeSlide={changeSlide}/>
                            </IonSlide>
                        </IonSlides>
                        
                        
                    </form>
                </div>
            </IonContent>
        </IonModal>
    );
}
 
export default CreateCardModal;