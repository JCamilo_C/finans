import { TypeAccountCards } from './constants';
export interface Config {
    maxValue: number;
    midValue: number;
    minValue: number;
}

export interface AccountInfo {
    bank: string;
    limit: number; 
    lastDigits: string
}

export interface CreditCardBankInfo {
    franchise: string;
    expire: string;
    cut: number;
    limit: number; 
    lastDigits: string
}

export interface DebitCardBankInfo {
    franchise?: string;
    expire?: string;
    bank: string
    lastDigits: string
    associateAccount?: ICard
}

export interface LoanInfo {
    createdAt: string;
    repeatOn: number;
    duration: number;
    quota: number;
    dues: number;
    fee: number;
    lender: string;
}

export interface ICard {
    accountType: TypeAccountCards;
    accountName: string;
    amount: number;
    currency: string;
    default: boolean;
    creditCard?: CreditCardBankInfo;
    loan?: LoanInfo;
    account?: AccountInfo;
    config?: Config;
}