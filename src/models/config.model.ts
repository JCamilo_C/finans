export interface UserConfig {
    uid?: string;
    localCurrency: string;
    firstName: string;
    lastName: string;
    avatar: string;
    localPass: string;
    email: string;
    locale: string;
    balanceAccount: number;
    status: 'Initial' | 'Validating' | 'Success' | 'Error';
}