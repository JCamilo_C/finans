export enum TypeAccountCards {
    ACCOUNT_TYPE = "ACCOUNT",
    CREDIT_TYPE = "CREDIT",
    CASH_TYPE = "CASH",
    LOAN_TYPE = "LOAN",
}

export enum TypeAcceptedCurrencies {
    AMERICA = "USD",
    COLOMBIAN = "COP",
    EUROPEAN = "EUR",
    BRITISH = "GBP",
    BRAZILIAN = "BRL",
}


export enum TypeFranchises {
    AMERICAN_EXPRESS = "AMX",
    MASTERCARD = "MASTERCARD",
    VISA = "VISA",
    DINNERS_CLUB = "DINNERS",
    OTHER = "OTHER",
}

export enum LogoFranchises {
    AMX = "amex.png",
    MASTERCARD = "mc.png",
    VISA = "visa.png",
    DINNERS = "dinners.png",
}

export const Avatar = [
    "avatar_1.png",
    "avatar_2.png",
    "avatar_3.png",
    "avatar_4.png",
    "avatar_5.png",
]

export const SelectRandomAvatar = () => {
    return Avatar[Math.floor(Math.random() * (5 - 0)) + 0];
}

export const nextStep = 'next';
export const prevStep = 'prev';


export const Validatiors = {
    Required: ()=> ({type: 'Required', data: null}),
    Email: (regex = null)=> ({type: 'Email', data: regex}),
    minLength: (length)=> ({type: 'minLength', data: length}),
    maxLength: (length)=> ({type: 'maxLength', data: length}),
    min: (length)=> ({type: 'min', data: length}),
    max: (length)=> ({type: 'max', data: length}),
    password: (regex)=> ({type: 'password', data: regex}),
    repeatPass: () => ({type: 'repeatPass', data: null})
}