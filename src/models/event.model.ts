
export interface Event {
    title: string;
    description: string;
    createdAt: Date;
    updatedAt: Date;
    goal: number;
    friends: string[]
}