

export interface Movement {
    title: string;
    decription: string;
    createdAt: Date;
    updatedAt: Date | null;
    recurrent: boolean;
    nextDate: Date | null;
    amount: number
}