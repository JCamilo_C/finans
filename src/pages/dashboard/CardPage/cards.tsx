import * as React from 'react';
import { connect } from 'react-redux';
import { IonReactRouter } from '@ionic/react-router';
import { IonPage  } from '@ionic/react';
import { Redirect, Route } from 'react-router-dom';
import { CreateCard } from './createCard/createCard.component';



const CardsPageComponent: React.FC<any> = () => {


    return (
        <IonPage>
            <IonReactRouter>
                <Route exact path="/cards/create">
                    <CreateCard />
                </Route>
            </IonReactRouter>
        </IonPage>
    )
}


export default connect(null,{})(CardsPageComponent)