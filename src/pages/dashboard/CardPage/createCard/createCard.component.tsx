import * as React from 'react';
import { IonHeader, IonToolbar, IonTitle, IonItem, IonContent, IonSelect, IonSelectOption } from '@ionic/react';
import './createCard.style.css'
import { TypeAccountCards, Validatiors } from '../../../../models/constants';

export const CreateCard: React.FC<any> = () => {

    const selectOpt = {
        cssClass: 'custom-alert-css',
        header: 'Seleccione un tipo de cuenta'
    }

    const [formLogin, setFormLogin] = React.useState({
        fields: {
            accountName: {
                value: "",
                validators: [Validatiors.Required()]
            }, 
            accountType: {
                value: "",
                validators: [Validatiors.Required()]
            }
        },
        errors: {
            email: [],
            password: []
        }
    })

    return (<IonContent>
        <IonHeader>
            <IonToolbar>
                <IonTitle>Create new card</IonTitle>
            </IonToolbar>
        </IonHeader>
        
        <form className="form-card-grid " style={{padding: '16px 5px'}}>
            <IonItem lines="none" style={{margin: '10px 0px'}}>
                <div className="form-control">
                    <label htmlFor="accountName" style={{marginBottom: '4px'}} >Nombre de cuenta</label>
                    <input id="accountName" placeholder="Nombre de cuenta" />
                </div>
            </IonItem>
            <IonItem lines="none" style={{margin: '10px 0px'}}>
                <div className="selection-grid">

                    <div className="form-control" style={{gridColumn: '1/2'}}>
                        <label style={{marginBottom: '4px'}}>Tipo de cuenta</label>
                    </div>

                        <IonSelect style={{gridColumn: '2/4', maxWidth: "100%"}} className="form-input" value={TypeAccountCards.ACCOUNT_TYPE} okText="Okay" cancelText="Dismiss" interfaceOptions={selectOpt} >
                            <IonSelectOption value={TypeAccountCards.ACCOUNT_TYPE}>Cuenta</IonSelectOption>
                            <IonSelectOption value={TypeAccountCards.CASH_TYPE}>Efectivo / Bolsillo</IonSelectOption>
                            <IonSelectOption value={TypeAccountCards.CREDIT_TYPE}>Tarjeta de crédito</IonSelectOption>
                            <IonSelectOption value={TypeAccountCards.LOAN_TYPE}>Préstamo</IonSelectOption>
                        </IonSelect>

                </div>
            </IonItem>
            
        </form>
    </IonContent>)
}

/**
 * <select placeholder="Nombre de cuenta" defaultValue={TypeAccountCards.ACCOUNT_TYPE}>
                        <option value={TypeAccountCards.ACCOUNT_TYPE}>Cuenta</option>
                        <option value={TypeAccountCards.CASH_TYPE}>Efectivo / Bolsillo</option>
                        <option value={TypeAccountCards.CREDIT_TYPE}>Tarjeta de crédito</option>
                        <option value={TypeAccountCards.DEBIT_TYPE}>Tarjeta de débito</option>
                    </select>
 */