import { IonContent, IonFab, IonFabButton, IonIcon, IonFabList, IonPage } from '@ionic/react';
import { addOutline, cardOutline, trendingDownOutline  } from 'ionicons/icons';
import './TabHome.css';

// COMPONENTS

import BalanceWidget from '../../../../components/molecules/BalanceWidget/balance-widget.component';

import { connect } from 'react-redux';
import { getCards } from '../../../../redux/selectors';
import { ICard } from '../../../../models/card.model';
import { AddMovement } from '../../../../redux/actions/movement.actions';
import { Movement } from '../../../../models/movement.model';
import { UserConfig } from 'src/models/config.model';
import CreateCardModal from 'src/modals/CreateCard/CreateCard.index';
import Header from 'src/components/molecules/Header';
import CardSlideWidget from '../../../../components/molecules/CardSlideWidget/CardSlideWidget';
import React from 'react';

interface TabProps  {
  cards: ICard[],
  userData: UserConfig,
  AddMovement: (move: Movement) => {}
}

const TabHome: React.FC<TabProps> = ({cards, userData, AddMovement}) => {
  const [openModal, setOpenModal] = React.useState(false)
  
  return (
    <IonPage>
      <IonContent fullscreen>
        <IonFab vertical="bottom" horizontal="end" slot="fixed">
          <IonFabButton>
            <IonIcon icon={addOutline} />
          </IonFabButton>
          <IonFabList side="top">
            <IonFabButton color="light" className="custom-fab">
              <IonIcon icon={addOutline} className="custom"></IonIcon> <span>Otro</span>
            </IonFabButton>
            <IonFabButton color="light" className="custom-fab">
              <IonIcon icon={trendingDownOutline} className="custom"></IonIcon> <span>Gasto</span>
            </IonFabButton>
            <IonFabButton color="light" className="custom-fab" onClick={() => setOpenModal(true)}>
              <IonIcon icon={cardOutline} className="custom"></IonIcon> <span>Tarjeta</span>
            </IonFabButton>
          </IonFabList>
        </IonFab>
        <Header userEmail={userData.email} userName={userData.firstName} avatar={userData.avatar} />
        <CardSlideWidget cards={cards} setOpenModal={setOpenModal} />
        <CreateCardModal openModal={openModal} setOpenModal={setOpenModal} localCurrency={userData.localCurrency} />
        <BalanceWidget  />
      </IonContent>
    </IonPage>
  );
};

const mapStateToProps = (state: any) => {
  return {
    cards: getCards(state),
    userData: state.config
  }
}

export default connect(mapStateToProps, {AddMovement})(TabHome);
