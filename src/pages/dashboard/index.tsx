import { Redirect, Route } from 'react-router-dom';
import {
  IonIcon,
  IonLabel,
  IonPage,
  IonRouterOutlet,
  IonTabBar,
  IonTabButton,
  IonTabs,
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { pieChart, walletOutline, settingsOutline } from 'ionicons/icons';


import TabHome from './Tabs/TabHome/TabHome';
import TabConfig from './Tabs/TabConfig/TabConfig';
import '../../theme/variables.css';

import '../../theme/global.css'

const App: React.FC = () => (
  <IonPage>
        <IonReactRouter>
            <IonTabs>
                <IonRouterOutlet>
                    <Route exact path="/dashboard/tabHome" component={TabHome} />
                    <Route exact path="/dashboard/tabConfig"  component={TabConfig} />
                    <Route exact path="/">
                        <Redirect to="/dashboard/tabHome" />
                    </Route>
                </IonRouterOutlet>
                <IonTabBar className="custom-tab-bar" slot="bottom">
                    <IonTabButton tab="tab1" href="/dashboard/tabHome">
                        <div className="tab-item">
                            <IonIcon icon={walletOutline}></IonIcon>
                            <IonLabel>Inicio</IonLabel>
                        </div>
                    </IonTabButton>
                    <IonTabButton tab="tab2" href="/dashboard/tabConfig">
                        <div className="tab-item">
                            <IonIcon icon={pieChart} />
                            <IonLabel>Tab 2</IonLabel>
                        </div>
                    </IonTabButton>
                    <IonTabButton tab="account" href="/dashboard/tabConfig">
                        <div className="tab-item">
                            <IonIcon icon={settingsOutline} />
                            <IonLabel>Cuenta</IonLabel>
                        </div>
                    </IonTabButton>
                </IonTabBar>
            </IonTabs>
        </IonReactRouter>
  </IonPage>
);

export default App;