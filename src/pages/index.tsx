import * as React from 'react';
import { connect } from 'react-redux';
import { AppState } from '../redux/app.reducer';
import { IntlProvider } from 'react-intl';
import AppLocale from '../lang';
import { IonReactRouter } from '@ionic/react-router';
import { IonRouterOutlet } from '@ionic/react';
import { Route, Redirect } from 'react-router-dom';
import { INTRODUCTION_PREFIX_PATH, HOME_PREFIX_PATH, APP_PREFIX_PATH } from '../config/AppConfig';

import IntroductionPage from './introduction';
import DashboardPage from './dashboard';


export interface PagesProps {
  locale: string;
  status: 'Initial' | 'Validating' | 'Success' | 'Error';
}

const Acomponent = ({isAuthenticated }) => {
  return (
    <Route
      exact
      path={APP_PREFIX_PATH}
      render={(props) => {
        return isAuthenticated === 'Success' ? (
          <Redirect
            to={{
              pathname: HOME_PREFIX_PATH,
            }}
          />
        ) : (
          <Redirect
            to={{
              pathname: INTRODUCTION_PREFIX_PATH,
            }}
          />);
      }}
    />
  );
}


const Pages: React.FC<PagesProps> = ({locale, status}) => {

    let currentAppLocale = AppLocale[locale];
    return (  
        <IntlProvider messages={currentAppLocale.messages} locale={currentAppLocale.locale}>
            <IonReactRouter>
              <IonRouterOutlet>
                <Route path="/introduction">
                  <IntroductionPage />
                </Route>
                <Route path="/dashboard">
                  <DashboardPage />
                </Route>
                <Acomponent isAuthenticated={status}></Acomponent>
              </IonRouterOutlet>
          </IonReactRouter>
        </IntlProvider>
    );
}

const mapStateProps = (state: AppState) => {
    const config = state.config;
    return {locale: config.locale, status: config.status }
}

 
export default connect(mapStateProps, {})(Pages);