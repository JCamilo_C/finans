import * as React from 'react';
import { connect, useDispatch } from 'react-redux';
import { setUserConfig, setUserLenguage, validateUser } from '../../redux/actions/config.actions';
import { UserConfig } from '../../models/config.model';
import { IonContent, IonPage, IonSlides, isPlatform, useIonToast } from '@ionic/react';
import './introduction.style.css'
import { StepOne } from './stepOne';
import StepTwo from './stepTwo/index';
import { Device } from '@capacitor/device';
import { useIntl } from 'react-intl';
import StepThree from './stepThree';
import { useHistory } from 'react-router';
interface interfaceIntroduction {
    config: UserConfig,
    setUserConfig: (config: UserConfig) => {},
    validateUser: (status: 'Initial' | 'Validating' | 'Success' | 'Error') => {},
    setUserLenguage: (lenguage: string) => {}
}

const Introduction: React.FC<interfaceIntroduction> = ({config, setUserConfig, validateUser, setUserLenguage}) => {
    const history = useHistory()
    const dispatch = useDispatch()
    const [present, dismiss] = useIonToast();
    const intl = useIntl();

    React.useEffect( () => {
        const GetLocale = async (): Promise<string> => {
            let lenguage = "";
          if (isPlatform('cordova') || isPlatform('capacitor')) {
            lenguage = (await Device.getLanguageCode()).value;
          } else {
            lenguage = navigator.language; // web. Not sur that the if is complete
          }
          if (lenguage.includes("es")) {
            setUserLenguage("es")
          } else {
            setUserLenguage("en")
          }
          return lenguage;
        }
        GetLocale();
    }, []);

    const slideOpts = {
        freeMode: false,
        initialSlide: 0,
        speed: 400,
        slidesPerView: 1,
        allowTouchMove: false
    };
    const slider = React.useRef<HTMLIonSlidesElement>(null)

    const handleChangeSlide = async (move: 'next' | 'prev') => {
        const swiper = await slider.current.getSwiper();
        if (move === 'next') swiper.slideNext()
        else if (move === 'prev') swiper.slidePrev()
    }

    const handleSaveUserData = async (data: UserConfig) => {
        dispatch(validateUser('Validating'))
        const swiper = await slider.current.getSwiper();
        swiper.slideNext()
        present({
            buttons: [{ text: intl.formatMessage({id: 'action.close'}), handler: () => dismiss() }],
            message: `<ion-icon name="cloud-upload-outline"></ion-icon> ${intl.formatMessage({id: 'form.creation.profile'})}`,
            duration: 2500,
            onDidDismiss: () => console.log('dismissed'),
            onWillDismiss: () => console.log('will dismiss'),
        })
        data.locale = config.locale;
        setUserConfig(data);
    }

    const goToDashboard = async () => {
        history.replace('/dashboard/tabHome') 
    }
      
    return (
        <IonPage>
            <IonContent fullscreen className="ion-padding background ">
                
                <IonSlides className="introduction" pager={true} options={slideOpts} ref={slider}>
                    <StepOne intl={intl} handleChangeSlide={handleChangeSlide} />
                    <StepTwo intl={intl} handleSaveUserData={handleSaveUserData} handleChangeSlide={handleChangeSlide} status={config.status} />
                    <StepThree intl={intl} goToDashboard={goToDashboard}  />
                </IonSlides>
            </IonContent>
        </IonPage>
    )
}

const mapStateProps = ({config}) => {
    return {config}
}

const mapDispatchProps = {
    setUserConfig,
    setUserLenguage,
    validateUser
}


export default React.memo(connect(mapStateProps, mapDispatchProps)(Introduction));