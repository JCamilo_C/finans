import styled from '@emotion/styled';

const IntroductionGrid = styled.div`
    display: flex;
    flex-direction: column;
    height: 80%;
    justify-content: space-between;
`
export default IntroductionGrid;

export const BtnArea = styled.div`
    display: flex;
    justify-content: flex-end;
    width: 100%; 
    margin-top: 5%;
`