import * as React from 'react';
import { FormattedMessage, IntlShape } from 'react-intl';
import IntroductionGrid from '../shared/Grid';
import './step-one.style.css'
import { BtnArea } from '../shared/Grid';
import { IntroductionSlide } from '../shared/Slide';

interface StepOneProps {
    handleChangeSlide: (move: 'next' | 'prev') => {},
    intl: IntlShape
}

export const StepOne: React.FC<StepOneProps> = ({handleChangeSlide, intl}) => {

    return (
        <IntroductionSlide>
            <div className="Logo">
                <div className="image-logo"></div>
                <h6 className="logo-text">Finans</h6>
            </div>
            <IntroductionGrid>
                <div id="target">
                    <img id="pattern" src="../../../assets/img/pattern.png" alt="Pattern" />
                </div>
                <h2 style={{textAlign: "left", fontSize: "20px", fontWeight: 'bold', padding: "10px 15px 0px 10px", marginTop: "15%"}}>
                    <FormattedMessage id="introduction.slide1.title" />
                </h2>
                <BtnArea>
                    <button className="primary next-page btn btn-introduction" onClick={e => handleChangeSlide('next')}>
                        <FormattedMessage id="action.start" />
                    </button>
                </BtnArea>
            </IntroductionGrid>
        </IntroductionSlide>
    )
}

