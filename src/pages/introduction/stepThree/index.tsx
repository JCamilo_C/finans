import './step-three.style.css';
import styled from '@emotion/styled';
import { IntlShape } from "react-intl";
import IntroductionGrid, { BtnArea } from "../shared/Grid";
import { IntroductionSlide } from '../shared/Slide';
export interface StepThreeProps {
    goToDashboard: () => {},
    intl: IntlShape
}

const CardDeck = styled.div`
    display: flex;
    margin: 0px -11rem;
`

const PreCard = styled.div`
    border-radius: 2rem;
    width: 23.5rem;
    height: 16rem;
    margin: 0 0.4rem;
    display: flex;
    justify-content: space-between;
    flex-direction: column;
    background-color: ${props => props.color};
`
 
const StepThree: React.FC<StepThreeProps> = ({goToDashboard, intl}) => {
    return ( 
        <IntroductionSlide>
            <div className="Logo">
                <div className="image-logo"></div>
                <h6 className="logo-text">Finans</h6>
            </div>
            <IntroductionGrid>
                <h2 style={{textAlign: "left", fontSize: "23px", fontWeight: 'bold', padding: "10px 15px 0px 10px", marginTop: "8%"}}>
                    {intl.formatMessage({id: 'introduction.slide3.title'})}
                </h2>
                <div id="target_final">
                    <CardDeck>
                        <PreCard color="#FF3D68">
                            <div style={{display: "flex", justifyContent: 'space-between', margin: '20px 12px'}}>
                                <div style={{width: '55%', height: '1.7rem', background: "#F8ACB1" , opacity: '0.8', borderRadius: '10px'}}></div>
                                <div style={{width: '2.5rem', height: '2.5rem', background: "#FFF7AE", opacity: '0.8', borderRadius: '5px'  }}></div>
                            </div>
                            <div style={{display: "flex", justifyContent: 'space-between', margin: '20px 12px', flexDirection: 'column'}}>
                                <div style={{width: '85%', height: '1.7rem', background: "#F8ACB1" , opacity: '0.8', borderRadius: '1rem', marginBottom: '0.5rem'}}></div>
                                <div style={{width: '55%', height: '1.7rem', background: "#F8ACB1", opacity: '0.8', borderRadius: '1rem'  }}></div>
                            </div>
                        </PreCard>
                        <PreCard color="#A73489">
                            <div style={{display: "flex", justifyContent: 'space-between', margin: '20px 12px'}}>
                                <div style={{width: '55%', height: '1.7rem', background: "#F8ACB1" , opacity: '0.8', borderRadius: '10px'}}></div>
                                <div style={{width: '2.5rem', height: '2.5rem', background: "#FFF7AE", opacity: '0.8', borderRadius: '5px'  }}></div>
                            </div>
                            <div style={{display: "flex", justifyContent: 'space-between', margin: '20px 12px', flexDirection: 'column'}}>
                                <div style={{width: '85%', height: '1.7rem', background: "#F8ACB1" , opacity: '0.8', borderRadius: '1rem', marginBottom: '0.5rem'}}></div>
                                <div style={{width: '55%', height: '1.7rem', background: "#F8ACB1", opacity: '0.8', borderRadius: '1rem'  }}></div>
                            </div>
                        </PreCard>
                    </CardDeck>
                </div>
                <BtnArea>
                    <button className="primary" onClick={() => goToDashboard()} style={{width: 'fit-content', height: '4.1rem', margin: 'auto 0'}}>
                        {intl.formatMessage({id: 'action.go.dashboard'})}
                    </button>
                </BtnArea>
            </IntroductionGrid>
            
            
        </IntroductionSlide>
     );
}
 
export default StepThree;