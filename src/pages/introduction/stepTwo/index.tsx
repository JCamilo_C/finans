import * as React from 'react';
import { IonSelect, IonSelectOption, useIonToast } from '@ionic/react';
import { SelectRandomAvatar, TypeAcceptedCurrencies } from '../../../models/constants';
import './step-two.style.css'
import { FormattedMessage, IntlShape } from 'react-intl';
import {IntroductionSlide} from '../shared/Slide';
import { UserConfig } from '../../../models/config.model';
import { useForm, defaultValidators } from 'react-reforms';
import IntroductionGrid from '../shared/Grid';
import { BtnArea } from '../shared/Grid';




export interface StepTwoProps {
    handleChangeSlide: (slide: string) => {};
    handleSaveUserData: (data: UserConfig) => {};
    status:  "Initial" | "Validating" | "Success" | "Error";
    intl: IntlShape;
}


 
const formStructure = {
    firstName: {
        value: "",
        validators: [defaultValidators.Required()],
        class: ''
    }, 
    lastName: {
        value: "",
        validators: [defaultValidators.Required()],
        class: ''
    },
    email: {
        value: "",
        validators: [defaultValidators.Required(), defaultValidators.Email(null)],
        class: ''
    }, 
    currency: {
        value: TypeAcceptedCurrencies.AMERICA,
        validators: [defaultValidators.Required()],
        class: ''
    },
}




const StepTwo: React.FC<StepTwoProps> = ({handleChangeSlide, handleSaveUserData, intl}) => {
    const [present, dismiss] = useIonToast();
    const {values, ValidateInput}= useForm(formStructure, {customClass: {error: 'error', success: 'success'}})

    const selectOpt = {
        cssClass: 'custom-alert-css',
        header: intl.formatMessage({id: "message.introduction.form.dropdown"})
    }

    const onSubmit = (e) => {
        e.preventDefault();
        const totalErrors = Object.values(values).reduce( (r, c) => r = r || c.hasErrors, false);
        const filled = Object.values(values).reduce( (r, c) =>  r = r || c.class === '' || c.class === null, false);
        if (totalErrors || filled) {
            present({
                buttons: [{ text: intl.formatMessage({id: "action.close"}), handler: () => dismiss() }],
                message: intl.formatMessage({id: 'form.error.1'}),
                duration: 3500
            })
            return;
        }

        let data: UserConfig = {
            localCurrency: values.currency.value,
            firstName: values.firstName.value,
            lastName: values.lastName.value,
            avatar: `../../../assets/img/${SelectRandomAvatar()}`,
            localPass: '',
            email: values.email.value,
            locale:  'es',
            balanceAccount: 0,
            status: 'Initial'
        }
        
        handleSaveUserData(data)
    }

    return ( 
        <IntroductionSlide>
            <div className="Logo">
                <div className="image-logo"></div>
                <h6 className="logo-text">Finans</h6>
            </div>
            <IntroductionGrid>
                <h2 style={{textAlign: "left", fontSize: "23px", fontWeight: 'bold', padding: "10px 15px 0px 10px", marginTop: "18%"}}>
                    <FormattedMessage id="introduction.slide2.title" />
                </h2>
                <form>
                    <div className="form-control" style={{margin: '5% 0'}}>
                        <input type="text" id="firstName" name="firstName" placeholder={intl.formatMessage({id: "introduction.form.names"}) } onChange={e => ValidateInput(e.target.name, e.target.value)} className={`${values.firstName.class}`} />
                    </div>
                    <div className="form-control" style={{margin: '5% 0'}}>
                        <input type="text" id="lastName" name="lastName" placeholder={intl.formatMessage({id: "introduction.form.lastnames"}) } onChange={e => ValidateInput(e.target.name, e.target.value)} className={`${values.lastName.class}`} />
                    </div>
                    <div className="form-control" style={{margin: '5% 0'}}>
                        <input type="email" id="email" name="email" placeholder={intl.formatMessage({id: "introduction.form.email"}) } onChange={e => ValidateInput(e.target.name, e.target.value)} className={`${values.email.class}`} />
                    </div>

                    <div className="selection-grid" style={{textAlign: 'left', margin: '4% 0'}}>

                        <div className="form-control" style={{gridColumn: '1/2'}}>
                            <label style={{marginBottom: '4px'}}><FormattedMessage id="introduction.form.currency" /></label>
                        </div>

                        <IonSelect id="currency" name="currency" style={{gridColumn: '2/4', maxWidth: "100%"}}  okText={intl.formatMessage({id: "action.pick"})} cancelText={intl.formatMessage({id: "action.close"})} interfaceOptions={selectOpt} onIonChange={e => ValidateInput('currency', e.detail.value)} className={`form-input ${values.currency.class}`} >
                            <IonSelectOption value={TypeAcceptedCurrencies.AMERICA}>USD</IonSelectOption>
                            <IonSelectOption value={TypeAcceptedCurrencies.BRAZILIAN}>{TypeAcceptedCurrencies.BRAZILIAN}</IonSelectOption>
                            <IonSelectOption value={TypeAcceptedCurrencies.BRITISH}>{TypeAcceptedCurrencies.BRITISH}</IonSelectOption>
                            <IonSelectOption value={TypeAcceptedCurrencies.COLOMBIAN}>{TypeAcceptedCurrencies.COLOMBIAN}</IonSelectOption>
                            <IonSelectOption value={TypeAcceptedCurrencies.EUROPEAN}>{TypeAcceptedCurrencies.EUROPEAN}</IonSelectOption>
                        </IonSelect>

                    </div>
                </form>
                <BtnArea>
                    <div style={{textAlign: 'left', width: '100%'}}>
                        <button type="button" className="link" onClick={e => handleChangeSlide('prev')} >
                            <FormattedMessage id="action.back" />
                        </button>
                    </div>
                    <div  style={{textAlign: 'right', width: '100%'}}>
                        <button type="button" className="primary"  onClick={e => onSubmit(e)} style={{right: '2%', width: "12rem"}}>
                            <FormattedMessage id="action.next" />
                        </button>
                    </div>
                </BtnArea>
            </IntroductionGrid>
        </IntroductionSlide>
    );
}
 
export default React.memo(StepTwo);