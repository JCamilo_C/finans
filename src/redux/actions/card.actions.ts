import { ICard } from '../../models/card.model';

export enum typesCardAction {
    CREATE_CARD = "[CARDS] Create new card",
    UPDATE_CARD = "[CARDS] Update card",
    REMOVE_CARD = "[CARDS] Remove card",
    CHANGE_CARD = "[CARDS] Change card",
}

export const ChangeCard = (numberCard: number) => ({
    type: typesCardAction.CHANGE_CARD,
    payload: numberCard
})

export const CreateNewCard = (card: ICard) => ({
    type: typesCardAction.CREATE_CARD,
    payload: card
})

export const UpdateCard = (id: number, card: ICard) => ({
    type: typesCardAction.UPDATE_CARD,
    payload: {id, card}
})

export const RemoveCard = (id: number) => ({
    type: typesCardAction.REMOVE_CARD,
    payload: id
})


