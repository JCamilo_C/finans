import { UserConfig } from '../../models/config.model';


export enum ConfigActionTypes {
    SET_CONFIG = "[CONFIG] set user config",
    SET_LENGUAGE_CONFIG = "[CONFIG] set user lenguage config",
    SET_STATUS_USER = "[CONFIG] set user status",
}



export const setUserConfig = (config: UserConfig) => ({
    type: ConfigActionTypes.SET_CONFIG,
    payload: config
})

export const validateUser = (status: 'Initial' | 'Validating' | 'Success' | 'Error', uid?: string) => ({
    type: ConfigActionTypes.SET_STATUS_USER,
    payload: {status, uid}
})

export const setUserLenguage = (lenguage: string) => ({
    type: ConfigActionTypes.SET_LENGUAGE_CONFIG,
    payload: lenguage
})

