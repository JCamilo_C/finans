import { Movement } from '../../models/movement.model';


export enum MovementTypeActions {
    ADD_MOVEMENT = '[MOVE] Create new movement',
    REMOVE_MOVEMENT = '[MOVE] Remove movement',
    UPDATE_MOVEMENT = '[MOVE] Update movement',
}

export const AddMovement = (move: Movement) => ({
    type: MovementTypeActions.ADD_MOVEMENT,
    payload: move
});

export const UpdateMovement = (id: number, move: Movement) => ({
    type: MovementTypeActions.UPDATE_MOVEMENT,
    payload: {id, move}
});

export const RemoveMovement = (id: number) => ({
    type: MovementTypeActions.REMOVE_MOVEMENT,
    payload: id
});