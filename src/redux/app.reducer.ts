import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas';

// PERSIST
import { persistReducer, persistStore, } from 'redux-persist';
import { CardState } from './reducers/card.reducer';
import { MovementState } from './reducers/movement.reducer';
import CapacitorStorage from 'redux-persist-capacitor';
import { UserConfig } from '../models/config.model';


declare global {
    interface Window {
      __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
  }
  

const persistConfig = {
    key: 'root',
    storage:  CapacitorStorage,
    blacklist: ['card']
}




export interface AppState {
    card: CardState,
    move: MovementState,
    config: UserConfig
}

const persistedReducer = persistReducer(persistConfig, reducers);
const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];

function configureStore(): {store: any, persistor: any} {
    // IF EXIST ON THE BROWSER ACTIVATE REDUX EXTENSION
    const composeEnchancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    // CONFIG STORE WITH MIDDLEWARES
    const store = createStore(persistedReducer, composeEnchancers(
        applyMiddleware(...middleware)
    ));
    
    // TURN ON EFFECTS
    sagaMiddleware.run(rootSaga)

    // ADD SYNC STORE
    let persistor = persistStore(store)

    return {store, persistor}
}

const store = configureStore();

export default store;