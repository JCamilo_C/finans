import { ICard } from '../../models/card.model';
import { TypeAccountCards, TypeFranchises } from '../../models/constants';
import { typesCardAction } from '../actions/card.actions';

export interface CardState {
    loading: boolean;
    cards: ICard[];
    cardSelected: number;
}

const initState: CardState = {
    loading: true,
    cards: [{
        accountName: 'Bolsillo',
        accountType: TypeAccountCards.CASH_TYPE,
        amount: 70000,
        currency: 'COP',
        default: true,
        config: {
            maxValue: 120000,
            midValue: 70000,
            minValue: 10000
        }
    },
    {
        accountName: 'Credito',
        accountType: TypeAccountCards.CREDIT_TYPE,
        amount: 5000000,
        currency: 'COP',
        default: true,
        creditCard: {
            cut: 16,
            expire: '08/22',
            franchise: TypeFranchises.AMERICAN_EXPRESS,
            lastDigits: '4562',
            limit: 7200000
        },
        config: {
            maxValue: 850000,
            midValue: 500000,
            minValue: 120000
        }
    },
    {
        accountName: 'Prestamo con el banco',
        accountType: TypeAccountCards.LOAN_TYPE,
        amount: 700,
        currency: 'USD',
        default: true,
        loan: {
            createdAt: "2021-06-20",
            repeatOn: 25,
            dues: 1200,
            quota: 100,
            duration: 12,
            fee: 0.012,
            lender: 'Bancolombia'
        }
    },
    {
        accountName: 'Cuenta General',
        accountType: TypeAccountCards.ACCOUNT_TYPE,
        amount: 18000000,
        currency: 'COP',
        default: true,
        config: {
            maxValue: 500000,
            midValue: 120000,
            minValue: 70000
        }
    }],
    cardSelected: 0
}


export function CardReducer(state = initState, action: {type: typesCardAction, payload?:  any  }): CardState {
    switch(action.type) {
        case typesCardAction.CHANGE_CARD:
            return {...state, cardSelected: action.payload };
        default:
            return state
    }
}
