import { UserConfig } from "../../models/config.model";
import { ConfigActionTypes } from "../actions/config.actions";

const initState: UserConfig = {
    uid: '',
    email: '',
    firstName: '',
    lastName: '',
    avatar: 'avatar_1.png',
    localCurrency: 'COP',
    localPass: null,
    locale: 'es',
    balanceAccount: 0,
    status: 'Initial'
}

export function ConfigReducer(state = initState, action: {type: ConfigActionTypes, payload?: any}) {
    switch (action.type) {
        case ConfigActionTypes.SET_CONFIG:
            return {...state, ...action.payload};
        case ConfigActionTypes.SET_LENGUAGE_CONFIG:
            return {...state, locale: action.payload};
        case ConfigActionTypes.SET_STATUS_USER:
            return {...state, status: action.payload.status, uid: action.payload.uid ?? state.uid};
        default:
            return state;
    }
}