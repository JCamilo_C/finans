import { combineReducers } from 'redux';
import { CardReducer } from './card.reducer'
import { MovementReducer } from './movement.reducer'
import CapacitorStorage from 'redux-persist-capacitor';
import { persistReducer } from 'redux-persist';
import { ConfigReducer } from './config.reducer';

const cardConfig = {
    key: 'card',
    storage:  CapacitorStorage,
    blacklist: ['cardSelected']
}

const reducers = combineReducers({
    card: persistReducer(cardConfig, CardReducer),
    move: MovementReducer,
    config: ConfigReducer
});


export default reducers;