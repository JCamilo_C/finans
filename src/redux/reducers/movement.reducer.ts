import { Movement } from '../../models/movement.model';
import { MovementTypeActions } from '../actions/movement.actions';

export interface MovementState {
    loading: boolean;
    moves: Movement[];
}


const initState: MovementState = {
    loading: true,
    moves: []
}

export function MovementReducer(state = initState, action: {type: MovementTypeActions, payload: any}): MovementState {
    switch (action.type) {
        case MovementTypeActions.ADD_MOVEMENT:
            return {
                ...state,
                moves: [...state.moves, action.payload]
            }
        case MovementTypeActions.UPDATE_MOVEMENT:
            return {
                ...state,
                moves: [...state.moves.map( (move, id: number) => {
                    if (id === action.payload.id) {
                        move = {...move, ...action.payload.move}
                    }
                    return move
                })
                ]
            }
        case MovementTypeActions.REMOVE_MOVEMENT:
            return {
                ...state,
                moves: [...state.moves.filter( (move, id: number) => id !== action.payload )]
            }
        default:
            return state;
    }
}