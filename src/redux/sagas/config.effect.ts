import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { ConfigActionTypes, validateUser } from '../actions/config.actions';
import { UserConfig } from '../../models/config.model';
import { createDoc } from '../../config/Firestore';

export function* LoadFirebaseData() {
    yield takeEvery(ConfigActionTypes.SET_CONFIG, function* ({payload}: {type: string, payload: UserConfig}) {
        try {
            const user = yield call(createDoc, payload);
            if (user) {
                yield put(validateUser('Success', user.id));
            } else {
                yield put(validateUser('Error'));
            }      
        } catch (error) {
            yield put(validateUser('Error'));
        }
      
    })
}

export default function* configSaga() {
    yield all([
        fork(LoadFirebaseData)
    ]);
}