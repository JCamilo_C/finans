import { all, fork } from "redux-saga/effects";
import Config from './config.effect';

export default function* rootSaga() {
  yield all([
    Config(),
  ]);
}
