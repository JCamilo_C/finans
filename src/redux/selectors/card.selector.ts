import { createSelector  } from 'reselect';
import { CardState } from '../reducers/card.reducer';

const getCardState = (state: any): CardState => state.card

export const getCards = createSelector(
    getCardState,
    (value: CardState) => value.cards
)

export const getSelectIndxCard = createSelector(
    getCardState,
    (value: CardState) => value.cardSelected
)


export const getSelectCard = createSelector(
    getCardState,
    (value: CardState) => {
        const card = value.cards[value.cardSelected]
        return card
    }
)