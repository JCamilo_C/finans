import { createSelector } from 'reselect';
import { AppState } from '../app.reducer';
import { UserConfig } from '../../models/config.model';

const selectConfig = (state: AppState): UserConfig => state.config;

export {}