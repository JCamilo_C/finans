import { AppState } from "../app.reducer";
import { createSelector } from 'reselect';
import { MovementState } from '../reducers/movement.reducer';


const getMovementState = (state: AppState) => state.move;

export const getMovesByDate =  (filter: Date) => createSelector(
    getMovementState,
    (moves: MovementState) => {
        const firstDay = new Date(filter.getFullYear(), filter.getMonth(), 1, 0, 0, 0);
        const lastDay = new Date(filter.getFullYear(), filter.getMonth() + 1, 0, 23, 59, 59);
        return moves.moves.map(move => ({...move, createdAt: new Date(move.createdAt)})).filter(move => {
            return move.createdAt >= firstDay && move.createdAt <= lastDay
        })
        
    }
)


export const getAmountMovesByDate = createSelector(
    getMovementState,
    (moves: MovementState) => {
        return moves.moves.reduce((response, move) => {
            response += move.amount;
            return response
        }, 0)
        
    }
)