interface UtilsInterface {
	getNameInitial(name: string): string;
	getColorContrast(hex: string): string;
	shadeColor(color: string, percent: number): string;
	rgbaToHex(rgba: string): string;
	getSignNum<T, K>(number: number, positive: T, negative: K): T | K | null;
	filterArray<T>(list: T[], key: string, value: T): T[];
	deleteArrayRow<T>(list: T[], key: string, value: T): T[];
}

class Utils {

	/**
	 * Get first character from first & last sentences of a username
	 * @param {String} name - Username
	 * @return {String} 2 characters string
	 */
	 static getNameInitial(name: string): string {
		let initials = name.match(/\b\w/g) || [];
		return ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
	}

	/**
	 * Get accessible color contrast
	 * @param {String} hex - Hex color code e.g '#3e82f7'
	 * @return {String} 'dark' or 'light'
	 */
	 static getColorContrast(hex: string): string {
		if(!hex) {
			return 'dark'
		}
		const threshold = 130;
		const hRed = hexToR(hex);
		const hGreen = hexToG(hex);
		const hBlue = hexToB(hex);
		function hexToR(h: string) {return parseInt((cutHex(h)).substring(0,2),16)}
		function hexToG(h: string) {return parseInt((cutHex(h)).substring(2,4),16)}
		function hexToB(h: string) {return parseInt((cutHex(h)).substring(4,6),16)}
		function cutHex(h: string) {return (h.charAt(0) === '#') ? h.substring(1,7):h}
		const cBrightness = ((hRed * 299) + (hGreen * 587) + (hBlue * 114)) / 1000;
		if (cBrightness > threshold){
			return 'dark'
		} else { 
			return 'light'
		}	
	}

	/**
	 * Darken or lighten a hex color 
	 * @param {String} color - Hex color code e.g '#3e82f7'
	 * @param {Number} percent - Percentage -100 to 100, positive for lighten, negative for darken
	 * @return {String} Darken or lighten color 
	 */
	static shadeColor(color: string, percent: number): string {
		let R = parseInt(color.substring(1,3),16);
		let G = parseInt(color.substring(3,5),16);
		let B = parseInt(color.substring(5,7),16);
		R = R * (100 + percent) / 100;
		G = G * (100 + percent) / 100;
		B = B * (100 + percent) / 100;
		R = (R<255)?R:255;  
		G = (G<255)?G:255;  
		B = (B<255)?B:255;  
		const RR = ((R.toString(16).length === 1) ? `0${R.toString(16)}` : R.toString(16));
		const GG = ((G.toString(16).length === 1) ? `0${G.toString(16)}` : G.toString(16));
		const BB = ((B.toString(16).length === 1) ? `0${B.toString(16)}` : B.toString(16));
		return `#${RR}${GG}${BB}`; 
	}

	/**
	 * Convert RGBA to HEX 
	 * @param {String} rgba - RGBA color code e.g 'rgba(197, 200, 198, .2)')'
	 * @return {String} HEX color 
	 */
	static rgbaToHex(rgba: string): string {
		const trim = (str: string) => (str.replace(/^\s+|\s+$/gm,''))
		const inParts = rgba.substring(rgba.indexOf("(")).split(","),
			r = parseInt(trim(inParts[0].substring(1)), 10),
			g = parseInt(trim(inParts[1]), 10),
			b = parseInt(trim(inParts[2]), 10),
			a = parseFloat(trim(inParts[3].substring(0, inParts[3].length - 1))).toFixed(2);
		const outParts = [
			r.toString(16),
			g.toString(16),
			b.toString(16),
			Math.round(parseFloat(a) * 255).toString(16).substring(0, 2)
		];

		outParts.forEach(function (part, i) {
			if (part.length === 1) {
				outParts[i] = '0' + part;
			}
		})
		return (`#${outParts.join('')}`);
	}

	/**
	 * Returns either a positive or negative 
	 * @param {Number} number - number value
	 * @param {any} positive - value that return when positive
	 * @param {any} negative - value that return when negative
	 * @return {any} positive or negative value based on param
	 */
	static getSignNum<T, K>(number: number, positive: T, negative: K): T | K | null {
		if (number > 0) {
			return positive
		}
		if (number < 0) {
			return negative
		}
		return null
	}

	/**
	 * Filter array of object 
	 * @param {Array} list - array of objects that need to filter
	 * @param {String} key - object key target
	 * @param {any} value  - value that excluded from filter
	 * @return {Array} a value minus b value
	 */
	static filterArray<T>(list: T[], key: string, value: T): T[] {
		let data = list
		if(list) {
			if (typeof list[0] === 'object') {
				data = list.filter((item: any) => item[key] === value)
			}
		}
		return data
	}

	/**
	 * Remove object from array by value
	 * @param {Array} list - array of objects
	 * @param {String} key - object key target
	 * @param {any} value  - target value
	 * @return {Array} Array that removed target object
	 */
	static deleteArrayRow<T>(list: T[], key: string, value: T): T[] {
		let data = list
		if(list) {
			if (typeof list[0] === 'object') {
				data = list.filter((item: any) => item[key] !== value)
			}
		}
		return data
	}

	static generateData(length: number, config: {min: number, max: number}): number[]  {
		return Array(length).fill((c: any) => c).map( () => (Math.floor(Math.random() * ((config.max + 1) - config.min)) + config.min) )
	}

	static generateRamdonNumberData(config: {min: number, max: number}): number  {
		return Math.floor(Math.random() * ((config.max + 1) - config.min)) + config.min
	}
	
}

export default Utils;